/*
 * Copyright 2019 BEBR.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.bebr.xdat.appoptics.metrics.alert;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import java.util.List;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
    @Type(value = AlertConditionAbove.class, name = "above"),
    @Type(value = AlertConditionAbsent.class, name = "absent"),
    @Type(value = AlertConditionBelow.class, name = "below")
})
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class AlertCondition {

    static final String TYPE_ABOVE = "above";
    static final String TYPE_ABSENT = "absent";
    static final String TYPE_BELOW = "below";

    private Long id;
    private String type;
    @JsonProperty("metric_name")
    private String metricName;
    private List<AlertTag> tags;
    @JsonProperty("detect_reset")
    private Boolean detectReset;
    private Integer duration;
    private String source;

    public AlertCondition() {
    }

    public AlertCondition(Long id, String type, String metricName, List<AlertTag> tags, Boolean detectReset, Integer duration, String source) {
        this.id = id;
        this.type = type;
        this.metricName = metricName;
        this.tags = tags;
        this.detectReset = detectReset;
        this.duration = duration;
        this.source = source;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMetricName() {
        return metricName;
    }

    public void setMetricName(String metricName) {
        this.metricName = metricName;
    }

    public List<AlertTag> getTags() {
        return tags;
    }

    public void setTags(List<AlertTag> tags) {
        this.tags = tags;
    }

    public Boolean getDetectReset() {
        return detectReset;
    }

    public void setDetectReset(Boolean detectReset) {
        this.detectReset = detectReset;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Override
    public String toString() {
        return "AlertCondition{" + "id=" + id + "type=" + type + ", metricName=" + metricName + ", tags=" + tags + ", detectReset=" + detectReset + ", duration=" + duration + ", source=" + source + '}';
    }

}
