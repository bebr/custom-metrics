/*
 * Copyright 2019 BEBR.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.bebr.xdat.appoptics.metrics;

import java.util.Objects;

/**
 *
 * @author Kilian Veenstra [kilian@corizon.nl]
 */
public class MetricTag {

    private String key;
    private String value;

    private static final String PROFILE_KEY = "environment";
    private static final String CLIENT_KEY = "client";

    public MetricTag(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    /**
     * This will create a profile tag
     *
     * @param profile add the profile variable, which should be the profile you
     * are running the job with.
     * @return returns a AppopticsMetricTag with key environment and the
     * parameter as value.
     */
    public static MetricTag profileTag(String profile) {
        return new MetricTag(PROFILE_KEY, profile);
    }

    /**
     * This will create a client tag
     *
     * @param client
     * @return returns a AppopticsMetricTag with key client and parameter as
     * value.
     */
    public static MetricTag clientTag(String client) {
        return new MetricTag(CLIENT_KEY, client);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.key);
        hash = 71 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MetricTag other = (MetricTag) obj;
        if (!Objects.equals(this.key, other.key)) {
            return false;
        }
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AppopticsMetricTag{" + "key=" + key + ", value=" + value + '}';
    }
}
