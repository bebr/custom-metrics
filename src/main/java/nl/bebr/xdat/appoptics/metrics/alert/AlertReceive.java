/*
 * Copyright 2019 BEBR.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.bebr.xdat.appoptics.metrics.alert;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.List;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder(value = {"id", "name", "description", "conditions", "services", "attributes", "active", "createdAt", "updatedAt", "version", "rearmSeconds", "rearmPerSignal", "md"})
public class AlertReceive {

    private Long id;
    private String name;
    private String description;
    private List<AlertCondition> conditions;
    @JsonProperty("services")
    private List<AlertReceiveService> services;
    private AlertAttribute attributes;
    private Boolean active;
    @JsonProperty("created_at")
    private Long createdAt;
    @JsonProperty("updated_at")
    private Long updatedAt;
    private Integer version;
    @JsonProperty("rearm_seconds")
    private Integer rearmSeconds;
    @JsonProperty("rearm_per_signal")
    private Boolean rearmPerSignal;
    private Boolean md;

    public AlertReceive() {
    }

    public AlertReceive(Long id, String name, String description, List<AlertCondition> conditions, List<AlertReceiveService> services, AlertAttribute attributes, Boolean active, Long createdAt, Long updatedAt, Integer version, Integer rearmSeconds, Boolean rearmPerSignal, Boolean md) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.conditions = conditions;
        this.services = services;
        this.attributes = attributes;
        this.active = active;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.version = version;
        this.rearmSeconds = rearmSeconds;
        this.rearmPerSignal = rearmPerSignal;
        this.md = md;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<AlertCondition> getConditions() {
        return conditions;
    }

    public void setConditions(List<AlertCondition> conditions) {
        this.conditions = conditions;
    }

    public List<AlertReceiveService> getServices() {
        return services;
    }

    public void setServices(List<AlertReceiveService> services) {
        this.services = services;
    }

    public AlertAttribute getAttributes() {
        return attributes;
    }

    public void setAttributes(AlertAttribute attributes) {
        this.attributes = attributes;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getRearmSeconds() {
        return rearmSeconds;
    }

    public void setRearmSeconds(Integer rearmSeconds) {
        this.rearmSeconds = rearmSeconds;
    }

    public Boolean getRearmPerSignal() {
        return rearmPerSignal;
    }

    public void setRearmPerSignal(Boolean rearmPerSignal) {
        this.rearmPerSignal = rearmPerSignal;
    }

    public Boolean getMd() {
        return md;
    }

    public void setMd(Boolean md) {
        this.md = md;
    }

    @Override
    public String toString() {
        return "AlertReceive{" + "id=" + id + ", name=" + name + ", description=" + description + ", conditions=" + conditions + ", services=" + services + ", attributes=" + attributes + ", active=" + active + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + ", version=" + version + ", rearmSeconds=" + rearmSeconds + ", rearmPerSignal=" + rearmPerSignal + ", md=" + md + '}';
    }

}
