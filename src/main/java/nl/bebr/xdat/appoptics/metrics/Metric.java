/*
 * Copyright 2019 BEBR.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.bebr.xdat.appoptics.metrics;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Kilian Veenstra [kilian@corizon.nl]
 */
public class Metric {

    private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
    private final List<MetricTag> tags = new ArrayList<>();
    private final String name;
    private Number value;

    private static final String RUN_DATE_KEY = "run_date";

    /**
     *
     * @param metricName metric name
     * @param metricValue metric value
     * @param profileTag this is the environment tag and will be with the value
     * of which profile you run the job with.
     * @param clientTag
     * @param tags metric specific tags, run_date tags will be added
     * automatically.
     */
    public Metric(String metricName, Number metricValue, MetricTag profileTag, MetricTag clientTag, MetricTag... tags) {
        this.tags.add(new MetricTag(RUN_DATE_KEY, dtf.format(LocalDate.now())));
        for (MetricTag tag : tags) {
            this.tags.add(tag);
        }

        this.tags.add(clientTag);
        this.tags.add(profileTag);

        this.name = metricName;
        this.value = metricValue;
    }

    public Number getValue() {
        return value;
    }

    public void setValue(Number value) {
        this.value = value;
    }

    public List<MetricTag> getTags() {
        return tags;
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.tags);
        hash = 17 * hash + Objects.hashCode(this.name);
        hash = 17 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Metric other = (Metric) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.tags, other.tags)) {
            return false;
        }
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AppopticsMetric{" + "tags=" + tags + ", name=" + name + ", value=" + value + '}';
    }

}
