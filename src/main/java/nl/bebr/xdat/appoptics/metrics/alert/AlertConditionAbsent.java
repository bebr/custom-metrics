/*
 * Copyright 2019 BEBR.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.bebr.xdat.appoptics.metrics.alert;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.List;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
@JsonPropertyOrder({"type", "metricName", "source", "tags", "detectReset", "duration"})
public class AlertConditionAbsent extends AlertCondition {

    public AlertConditionAbsent() {
    }

    public AlertConditionAbsent(Long id, String metricName, List<AlertTag> tags, Boolean detectReset, Integer duration, String source) {
        super(id, TYPE_ABSENT, metricName, tags, detectReset, duration, source);
    }

    @Override
    public String toString() {
        return super.toString() + " AlertConditionAbsent{" + '}';
    }

}
