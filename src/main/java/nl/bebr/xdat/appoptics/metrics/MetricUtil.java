/*
 * Copyright 2019 BEBR.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.bebr.xdat.appoptics.metrics;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.logging.Level;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl], Kilian Veenstra
 * [kilian@corizon.nl]
 */
public class MetricUtil {

    private static final Logger log = LoggerFactory.getLogger(MetricUtil.class);

    private static final String APP_OPTICS_URL = "https://api.appoptics.com/v1/measurements";

    private final String apiToken;

    public MetricUtil(String apiToken) {
        this.apiToken = apiToken;
    }

    //
    // with the AppOptic token the password can be an empty String.
    //
    private final String password = "";

    private static final String METRIC_NAME_KEY = "name";
    private static final String METRIC_VALUE_KEY = "value";
    private static final String TAGS_LIST_KEY = "tags";
    private static final String MEASUREMENTS_LIST_KEY = "measurements";

    private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");

    public void sendMetric(Metric metric) throws MalformedURLException, IOException {
        HttpPost httpPost = createConnectivity(APP_OPTICS_URL, apiToken, password);
        executeReq(metricToJSON(metric), httpPost);
    }

    public void sendMetric(List<Metric> metricList) {
        metricList.forEach((metric) -> {
            try {
                sendMetric(metric);
                //
                // Sleep is added to give AppOptics time to process the request
                //
                Thread.sleep(10000L);
            } catch (InterruptedException | IOException ex) {
                java.util.logging.Logger.getLogger(MetricUtil.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    JSONObject metricToJSON(Metric metric) {
        //
        // Convert the incoming metric to a JSONObject.
        // List with tags will be a JSONObject inside the main Object.
        // List with measurements will be a difirent JSONObject inside the main object,
        // this is a list because AppOptics expects this variable as a list.
        //
        JSONObject jsonMetric = new JSONObject();
        JSONObject tags = new JSONObject();
        metric.getTags().forEach((t) -> tags.put(t.getKey(), t.getValue()));
        JSONObject measurementsObject = new JSONObject();
        measurementsObject.put(METRIC_NAME_KEY, metric.getName());
        measurementsObject.put(METRIC_VALUE_KEY, metric.getValue());
        JSONArray measurementsArray = new JSONArray();
        measurementsArray.add(measurementsObject);
        jsonMetric.put(TAGS_LIST_KEY, tags);
        jsonMetric.put(MEASUREMENTS_LIST_KEY, measurementsArray);
        return jsonMetric;
    }

    /**
     * Create connection with AppOptics
     *
     * @param restUrl the AppOpptics url.
     * @param username this is the AppOptic token, which we use as username.
     * @param password with the AppOptic token this can be an empty String.
     * @return returns a HttPost Object to connect with AppOpptics.
     */
    private HttpPost createConnectivity(String restUrl, String username, String password) {
        HttpPost post = new HttpPost(restUrl);
        //
        // set the username and password
        // set the request headers
        //
        String auth = new StringBuffer(username).append(":").append(password).toString();
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
        String authHeader = "Basic " + new String(encodedAuth);
        post.setHeader("AUTHORIZATION", authHeader);
        post.setHeader("Content-Type", "application/json");
        post.setHeader("Accept", "application/json");
        post.setHeader("X-Stream", "true");
        return post;
    }

    private void executeReq(JSONObject jsonData, HttpPost httpPost) {
        try {
            executeHttpRequest(jsonData, httpPost);
        } catch (UnsupportedEncodingException e) {
            log.error("error while encoding api url : " + e);
        } catch (IOException e) {
            log.error("ioException occured while sending http request : " + e);
        } catch (Exception e) {
            log.error("exception occured while sending http request : " + e);
        } finally {
            httpPost.releaseConnection();
        }
    }

    /**
     * Makes connection to AppOptics and send the data to create/update metrics.
     *
     * @param jsonData the JSONObject created by buildQueryData() that will be
     * send to AppOptics.
     * @param httpPost the HttpPost Object created by createConnectivity() to
     * connect to AppOptics.
     * @throws UnsupportedEncodingException
     * @throws IOException
     */
    private void executeHttpRequest(JSONObject jsonData, HttpPost httpPost) throws UnsupportedEncodingException, IOException {
        HttpResponse response = null;
        String line = "";
        //
        // Create StringBuilder to print out the result of the send information.
        //
        StringBuilder result = new StringBuilder();
        //
        // Set the JSONObject data to a StringEntity and
        // create the HttpClient to execute the connection.
        //
        httpPost.setEntity(new StringEntity(jsonData.toJSONString()));
        HttpClient httpClient = HttpClientBuilder.create().build();
        response = httpClient.execute(httpPost);
        log.info("Post parameters : " + jsonData.toJSONString());
        log.info("Response Code : " + response.getStatusLine().getStatusCode());
        BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        //
        // Get the result of the send data.
        //
        while ((line = reader.readLine()) != null) {
            result.append(line);
        }
        log.info(result.toString());
    }
}
