/*
 * Copyright 2019 BEBR.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.bebr.xdat.appoptics.metrics.alert;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import nl.bebr.xdat.appoptics.metrics.Metric;
import nl.bebr.xdat.appoptics.metrics.MetricTag;
import nl.bebr.xdat.appoptics.metrics.MetricUtil;
import nl.bebr.xdat.appoptics.metrics.ResponseMessage;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.modelmapper.Conditions;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import static org.modelmapper.convention.MatchingStrategies.STRICT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
public class AlertUtil {

    private static final Logger log = LoggerFactory.getLogger(AlertUtil.class);

    private static final Double DEFAULT_METRIC_VALUE = Math.PI;
    private static final String GET_URL = "https://api.appoptics.com/v1/alerts?name=";
    private static final String POST_URL = "https://api.appoptics.com/v1/alerts";
    private static final String PUT_URL = "https://api.appoptics.com/v1/alerts/";
    private static final String DELETE_URL = "https://api.appoptics.com/v1/alerts/";
    private static final String EMPTY_PASSWORD = "";

    private final String readToken;
    private final String writeToken;

    private final MetricUtil metricUtil;
    private final ObjectMapper jsonMapper;
    private final ModelMapper objectMapper;
    private final HttpClient httpClient;

    public AlertUtil(String apiToken, String readToken, String writeToken) {
        this.readToken = readToken;
        this.writeToken = writeToken;
        metricUtil = new MetricUtil(apiToken);
        jsonMapper = new ObjectMapper();
        httpClient = HttpClientBuilder.create().disableContentCompression().build();

        //
        // Set up object mapper
        // Provide converter from receiveService entities to longs
        //
        Converter<List<AlertReceiveService>, List<Long>> servicesConverter = ctx -> ctx.getSource() == null ? null : ctx.getSource().stream()
                .map(serviceEntity -> serviceEntity.getId())
                .collect(Collectors.toList());

        objectMapper = new ModelMapper();
        objectMapper.getConfiguration().setMatchingStrategy(STRICT);
        objectMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        objectMapper.createTypeMap(AlertReceive.class, AlertSend.class)
                .addMappings(mapper -> mapper.using(servicesConverter).map(AlertReceive::getServices, AlertSend::setServices));

    }

    /**
     * Checks if alert with provided name exists.
     *
     * @param alertName
     * @return
     */
    public boolean alertExists(String alertName) {
        return getAlert(alertName) != null;
    }

    /**
     * This method tries to retrieve an Alert from appoptics with provided name.
     *
     * @param alertName
     * @return
     */
    public AlertReceive getAlert(String alertName) {
        log.info("Alert: {} - GET", alertName);

        try {
            ResponseMessage message = executeHttpGet(alertName);
            if (message != null && message.getAlerts() != null && !message.getAlerts().isEmpty()) {
                return message.getAlerts().get(0);
            } else {
                log.warn("Alert: {} - Could not be found", alertName);
            }
        } catch (IOException ex) {
            log.error("{}", ex); //< improve
        }

        return null;
    }

    /**
     * This method tries to delete an Alert from appoptics with provided name.
     *
     * @param alertName
     */
    public void deleteAlert(String alertName) {
        log.info("Alert: {} - DELETE", alertName);

        AlertReceive alert = getAlert(alertName);
        if (alert != null) {
            try {
                executeHttpDelete(alert.getId());
            } catch (IOException ex) {
                log.error("{}", ex); //< improve
            }
        } else {
            log.warn("Alert: {} - Not deleting", alertName);
        }
    }

    /**
     * This method tries to update an Alert from appoptics with provided Alert
     * values. Note: you need to supply the Alert with an existing id for this
     * method to work properly.
     *
     * @param alert
     */
    public void updateAlert(AlertSend alert) {
        log.info("Alert: {} - UPDATE", alert.getName());
        log.debug("Alert: {}", alert);
        if (alert.getId() == null) {
            throw new IllegalArgumentException("Alert id must be filled to be able to update");
        }

        try {
            executeHttpPut(alert);
        } catch (IOException ex) {
            log.error("{}", ex); //< improve
        }

    }

    /**
     * This method tries to create an Alert with provided values. Client and
     * profile are needed to send an empty metric prior to pushing the alert.
     *
     * @param alert
     * @param client
     * @param profile
     */
    public void createAlert(AlertSend alert, String client, String profile) {
        log.info("Alert: {} - CREATE", alert.getName());
        log.debug("Alert: {}", alert);

        MetricTag clientTag = MetricTag.clientTag(client);
        MetricTag profileTag = MetricTag.profileTag(profile);

        alert.getConditions().forEach(condition -> {

            Metric metric = new Metric(condition.getMetricName(), DEFAULT_METRIC_VALUE, profileTag, clientTag);

            try {
                metricUtil.sendMetric(metric);
            } catch (IOException ex) {
                log.error("{}", ex); //< improve
            }

        });
        try {
            AlertReceive sent = executeHttpPost(alert);
            log.debug("Sent Alert: {}", jsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(sent));
        } catch (IOException ex) {
            log.error("{}", ex); //< improve
        }

    }

    /**
     * Sets Alert's 'active' property to true and pushes to Appoptics.
     *
     * @param alertName
     */
    public void enableAlert(String alertName) {
        log.info("Alert: {} - ENABLE", alertName);

        AlertReceive alert = getAlert(alertName);
        if (alert != null) {
            alert.setActive(true);
            updateAlert(convertToSend(alert));
        } else {
            log.warn("Alert: {} - Not enabling", alertName);
        }
    }

    /**
     * Sets Alert's 'active' property to false and pushes to Appoptics.
     *
     * @param alertName
     */
    public void disableAlert(String alertName) {
        log.info("Alert: {} - DISABLE", alertName);

        AlertReceive alert = getAlert(alertName);
        if (alert != null) {
            alert.setActive(false);
            updateAlert(convertToSend(alert));
        } else {
            log.warn("Alert: {} - Not disabling", alertName);
        }
    }

    /**
     * Util method to post provided Alert. Used by createAlert.
     *
     * @param alert
     * @throws UnsupportedEncodingException
     * @throws IOException
     * @return
     */
    private AlertReceive executeHttpPost(AlertSend alert) throws UnsupportedEncodingException, IOException {
        HttpPost httpPost = new HttpPost(POST_URL);
        setAuthHeader(httpPost, writeToken);

        httpPost.setHeader("Accept", "*/*");
        httpPost.setHeader("Content-Type", "application/json");

        String alertString = jsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(alert);

        httpPost.setEntity(new StringEntity(alertString));

        String response = executeUriRequest(httpPost);

        return jsonMapper.readValue(response, AlertReceive.class);
    }

    /**
     * Util method to put provided Alert. Used by updateAlert.
     *
     * @param alert
     * @throws UnsupportedEncodingException
     * @throws IOException
     */
    private void executeHttpPut(AlertSend alert) throws UnsupportedEncodingException, IOException {
        HttpPut httpPut = new HttpPut(PUT_URL + alert.getId());
        setAuthHeader(httpPut, writeToken);

        httpPut.setHeader("Accept", "*/*");
        httpPut.setHeader("Content-Type", "application/json");

        String alertString = jsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(alert);

        httpPut.setEntity(new StringEntity(alertString));

        HttpResponse response = httpClient.execute(httpPut);
        log.debug("-  Response Code: {}", response.getStatusLine().getStatusCode());

    }

    /**
     * Util method to get Alert with provided name. Used by getAlert.
     *
     * @param alert
     * @throws UnsupportedEncodingException
     * @throws IOException
     * @return
     */
    private ResponseMessage executeHttpGet(String alertName) throws UnsupportedEncodingException, IOException {
        HttpGet httpGet = new HttpGet(GET_URL + alertName);
        setAuthHeader(httpGet, readToken);

        httpGet.setHeader("Content-Type", "application/json");
        httpGet.setHeader("Accept", "application/json");
        httpGet.setHeader("X-Stream", "true");

        String response = executeUriRequest(httpGet);

        ResponseMessage message = jsonMapper.readValue(response, ResponseMessage.class);

        return message;
    }

    /**
     * Util method to delete Alert with provided id. Used by deleteAlert.
     *
     * @param alert
     * @throws IOException
     * @return
     */
    private void executeHttpDelete(Long alertId) throws IOException {
        HttpDelete httpDelete = new HttpDelete(DELETE_URL + alertId);
        setAuthHeader(httpDelete, writeToken);

        HttpResponse response = httpClient.execute(httpDelete);
        log.debug("-  Response Code: {}", response.getStatusLine().getStatusCode());
    }

    /**
     * Util method to execute provided HttpUriRequest. Used by executeHttpPost
     * and executeHttpGet.
     *
     * @param request
     * @return
     * @throws IOException
     */
    private String executeUriRequest(HttpUriRequest request) throws IOException {
        HttpResponse response = httpClient.execute(request);
        log.debug("-  Response Code: {}", response.getStatusLine().getStatusCode());

        String text = null;
        try (Scanner scanner = new Scanner(response.getEntity().getContent(), StandardCharsets.UTF_8.name())) {
            text = scanner.useDelimiter("\\A").next();
        }
        return text;

    }

    /**
     * Util method to set auth header to provided HttpUriRequest. Used by
     * execute methods.
     *
     * @param request
     * @param token
     */
    private void setAuthHeader(HttpUriRequest request, String token) {
        String auth = new StringBuffer(token).append(":").append(EMPTY_PASSWORD).toString();
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
        String authHeader = "Basic " + new String(encodedAuth);
        request.setHeader("AUTHORIZATION", authHeader);
    }

    /**
     * Copies values from provided alertReceive to a new AlertSend object.
     *
     * @param alertReceive
     * @return
     */
    AlertSend convertToSend(AlertReceive alertReceive) {
        return objectMapper.map(alertReceive, AlertSend.class);
    }
}
