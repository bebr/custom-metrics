/*
 * Copyright 2019 BEBR.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.bebr.xdat.appoptics.metrics.alert;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.Map;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AlertReceiveService {

    private Long id;
    private String type;
    private Map<String, String> settings;
    private String title;

    public AlertReceiveService(Long id, String type, Map<String, String> settings, String title) {
        this.id = id;
        this.type = type;
        this.settings = settings;
        this.title = title;
    }

    public AlertReceiveService(Long id) {
        this.id = id;
    }

    public AlertReceiveService() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, String> getSettings() {
        return settings;
    }

    public void setSettings(Map<String, String> settings) {
        this.settings = settings;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "AlertService{" + "id=" + id + ", type=" + type + ", settings=" + settings + ", title=" + title + '}';
    }

}
