/*
 * Copyright 2019 BEBR.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.bebr.xdat.appoptics.metrics.alert;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AlertTag {

    static final String CLIENT_TAG = "client";
    static final String PROFILE_TAG = "environment";

    String name;
    Boolean grouped;
    List<String> values;

    public AlertTag(String name, Boolean grouped, List<String> values) {
        this.name = name;
        this.grouped = grouped;
        this.values = values;
    }

    /**
     * This method is provided to shorten the way of creating the frequently
     * used profile tag.
     *
     * @param profile
     * @return
     */
    public static AlertTag profileTag(String profile) {
        return new AlertTag(PROFILE_TAG, null, Collections.singletonList(profile));
    }

    /**
     * This method is provided to shorten the way of creating the frequently
     * used client tag.
     *
     * @param client
     * @return
     */
    public static AlertTag clientTag(String client) {
        return new AlertTag(CLIENT_TAG, null, Collections.singletonList(client));
    }

    public AlertTag() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getGrouped() {
        return grouped;
    }

    public void setGrouped(Boolean grouped) {
        this.grouped = grouped;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    @Override
    public String toString() {
        return "AlertTag{" + "name=" + name + ", grouped=" + grouped + ", values=" + values + '}';
    }

}
