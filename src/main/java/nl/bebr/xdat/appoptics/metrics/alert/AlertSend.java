/*
 * Copyright 2019 BEBR.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.bebr.xdat.appoptics.metrics.alert;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.List;
import java.util.Optional;
import static nl.bebr.xdat.appoptics.metrics.alert.AlertTag.CLIENT_TAG;
import static nl.bebr.xdat.appoptics.metrics.alert.AlertTag.PROFILE_TAG;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder(value = {"id", "name", "description", "conditions", "services", "attributes", "active", "rearmSeconds", "rearmPerSignal", "md"})
public class AlertSend {

    private Long id;
    private String name;
    private String description;
    private List<AlertCondition> conditions;
    @JsonProperty("services")
    private List<Long> services;
    private AlertAttribute attributes;
    private Boolean active;
    @JsonProperty("rearm_seconds")
    private Integer rearmSeconds;
    @JsonProperty("rearm_per_signal")
    private Boolean rearmPerSignal;
    private Boolean md;

    public AlertSend() {
    }

    public AlertSend(Long id, String name, String description, List<AlertCondition> conditions, List<Long> services, AlertAttribute attributes, Boolean active, Integer rearmSeconds, Boolean rearmPerSignal, Boolean md) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.conditions = conditions;
        this.services = services;
        this.attributes = attributes;
        this.active = active;
        this.rearmSeconds = rearmSeconds;
        this.rearmPerSignal = rearmPerSignal;
        this.md = md;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Long> getServices() {
        return services;
    }

    public void setServices(List<Long> services) {
        this.services = services;
    }

    public AlertSend(String name) {
        this.name = name;
    }

    public Integer getRearmSeconds() {
        return rearmSeconds;
    }

    public void setRearmSeconds(Integer rearmSeconds) {
        this.rearmSeconds = rearmSeconds;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getRearmPerSignal() {
        return rearmPerSignal;
    }

    public void setRearmPerSignal(Boolean rearmPerSignal) {
        this.rearmPerSignal = rearmPerSignal;
    }

    public Boolean getMd() {
        return md;
    }

    public void setMd(Boolean md) {
        this.md = md;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<AlertCondition> getConditions() {
        return conditions;
    }

    public void setConditions(List<AlertCondition> conditions) {
        this.conditions = conditions;
    }

    public AlertAttribute getAttributes() {
        return attributes;
    }

    public void setAttributes(AlertAttribute attributes) {
        this.attributes = attributes;
    }

    String findTag(String key) {
        if (conditions != null && !conditions.isEmpty()) {
            Optional<String> queryTag = conditions
                    .stream()
                    .flatMap(c -> c.getTags().stream())
                    .filter(t -> key.equals(t.getName()))
                    .flatMap(t -> t.getValues().stream())
                    .findFirst();
            if (queryTag.isPresent()) {
                return queryTag.get();
            }
        }
        return null;
    }

    @JsonIgnore
    public String getClient() {
        return findTag(CLIENT_TAG);
    }

    @JsonIgnore
    public String getProfile() {
        return findTag(PROFILE_TAG);
    }

    @Override
    public String toString() {
        return "AlertSend{" + "id=" + id + ", name=" + name + ", description=" + description + ", conditions=" + conditions + ", services=" + services + ", attributes=" + attributes + ", active=" + active + ", rearmSeconds=" + rearmSeconds + ", rearmPerSignal=" + rearmPerSignal + ", md=" + md + '}';
    }

}
