/*
 * Copyright 2019 BEBR.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.bebr.xdat.appoptics.metrics;

import nl.bebr.xdat.appoptics.metrics.alert.AlertReceive;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
@JsonIgnoreProperties(value = {"query"})
public class ResponseMessage {

    private List<AlertReceive> alerts;

    public ResponseMessage(List<AlertReceive> alerts) {
        this.alerts = alerts;
    }

    public ResponseMessage() {
    }

    public List<AlertReceive> getAlerts() {
        return alerts;
    }

    public void setAlerts(List<AlertReceive> alerts) {
        this.alerts = alerts;
    }

    @Override
    public String toString() {
        return "ResponseMessage{" + "alerts=" + alerts + '}';
    }

}
