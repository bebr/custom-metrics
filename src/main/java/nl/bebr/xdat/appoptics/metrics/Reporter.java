/*
 * Copyright 2020 BEBR.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.bebr.xdat.appoptics.metrics;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kilian Veenstra [kilian@corizon.nl]
 */
public class Reporter {

    private static final Logger LOG = LoggerFactory.getLogger(Reporter.class);

    private final String profile;
    private final String client;
    private final MetricUtil metricUtil;

    public Reporter(String appopticsToken, String profile, String client) {
        this.profile = profile;
        this.client = client;
        this.metricUtil = new MetricUtil(appopticsToken);
    }

    /**
     *
     * Sends reports to Appoptics, as value it sends the amount of data gathered
     *
     * @param inputMetricName Name of the Appoptics metric
     * @param metricValue Value for the metric
     */
    public void sendInputReport(String inputMetricName, int metricValue) {
        sendReport(inputMetricName, metricValue);
    }

    /**
     *
     * Sends reports to Appoptics, as value it sends the amount of data gathered
     *
     * @param outputMetricName Name of the Appoptics metric
     * @param metricValue Value for the metric
     */
    public void sendOutputReport(String outputMetricName, int metricValue) {
        sendReport(outputMetricName, metricValue);
    }

    private void sendReport(String metricName, int count) {
        LOG.debug("Sending report for client: {}, profile: {}, metric: {}, count: {}", client, profile, metricName, count);

        MetricTag clientTag = MetricTag.clientTag(client);
        MetricTag profileTag = MetricTag.profileTag(profile);

        Metric metric = new Metric(metricName, count, profileTag, clientTag);

        try {
            metricUtil.sendMetric(metric);
        } catch (IOException ex) {
            LOG.error("IOException while sending metric to AppOptics", ex);
        }
    }

}
