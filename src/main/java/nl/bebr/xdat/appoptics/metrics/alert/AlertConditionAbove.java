/*
 * Copyright 2019 BEBR.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.bebr.xdat.appoptics.metrics.alert;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.List;
import nl.bebr.xdat.appoptics.metrics.SummaryFunction;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
@JsonPropertyOrder({"type", "metricName", "source", "threshold", "summaryFunction", "tags", "detectReset", "duration"})
public class AlertConditionAbove extends AlertCondition {

    //measurements over this number will fire the alert.
    private Double threshold;
    //string: Indicates which statistic of an aggregated measurement to alert on. 
    @JsonProperty("summary_function")
    private SummaryFunction summaryFunction;

    public AlertConditionAbove() {
    }

    public AlertConditionAbove(Double threshold, SummaryFunction summaryFunction, Long id, String metricName, List<AlertTag> tags, Boolean detectReset, Integer duration, String source) {
        super(id, TYPE_ABOVE, metricName, tags, detectReset, duration, source);
        this.threshold = threshold;
        this.summaryFunction = summaryFunction;
    }

    public Double getThreshold() {
        return threshold;
    }

    public void setThreshold(Double threshold) {
        this.threshold = threshold;
    }

    public String getSummaryFunction() {
        return summaryFunction.name().toLowerCase();
    }

    public void setSummaryFunction(String sfString) {
        this.summaryFunction = SummaryFunction.valueOf(sfString.toUpperCase());
    }

    @Override
    public String toString() {
        return super.toString() + " AlertConditionAbove{" + "threshold=" + threshold + ", summaryFunction=" + summaryFunction + '}';
    }

}
