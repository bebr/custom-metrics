/*
 * Copyright 2019 BEBR.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.bebr.xdat.appoptics.metrics.alert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import static nl.bebr.xdat.appoptics.metrics.alert.AlertTag.CLIENT_TAG;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
public class AlertSendTest {

    public AlertSendTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of findTag method, of class Alert.
     */
    @Test
    public void testFindTag() {
        System.out.println("findTag");

        AlertSend alert = new AlertSend();
        List<AlertCondition> conditions = new ArrayList<>();
        List<AlertTag> tags1 = new ArrayList<>();
        tags1.add(new AlertTag("tag_name", null, Collections.singletonList("tag_value")));
        tags1.add(new AlertTag(CLIENT_TAG, null, Arrays.asList(new String[]{"testCLIENT", "koekjesClient"})));
        List<AlertTag> tags2 = new ArrayList<>();
        tags2.add(AlertTag.profileTag("testPROFILE"));

        conditions.add(new AlertConditionAbove(null, null, null, null, tags1, null, null, null));
        conditions.add(new AlertConditionAbove(null, null, null, null, tags2, null, null, null));
        alert.setConditions(conditions);

        String expClient = "testCLIENT";
        String client = alert.getClient();
        assertEquals(expClient, client);

        String expProfile = "testPROFILE";
        String profile = alert.getProfile();
        assertEquals(expProfile, profile);
    }

}
