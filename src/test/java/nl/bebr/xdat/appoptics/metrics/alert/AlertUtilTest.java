/*
 * Copyright 2019 BEBR.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.bebr.xdat.appoptics.metrics.alert;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static nl.bebr.xdat.appoptics.metrics.alert.AlertTag.CLIENT_TAG;
import static nl.bebr.xdat.appoptics.metrics.alert.AlertTag.PROFILE_TAG;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
public class AlertUtilTest {

    public AlertUtilTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of convertToSend method, of class AppopticsAlertUtil.
     */
    @Test
    public void testConvertToSend() {
        System.out.println("convertToSend");
        AlertUtil instance = new AlertUtil(null, null, null);

        AlertReceive alertReceive = new AlertReceive();
        alertReceive.setActive(true);
        alertReceive.setAttributes(new AlertAttribute("run_book"));

        List<AlertCondition> conditions = new ArrayList<>();
        List<AlertTag> tags = new ArrayList<>();
        tags.add(new AlertTag("the_tag", null, Collections.singletonList("the_tag_value")));
        tags.add(AlertTag.clientTag("the_client"));
        tags.add(AlertTag.profileTag("the_profile"));
        conditions.add(new AlertConditionAbsent(null, "testMetric", tags, null, null, null));

        alertReceive.setConditions(conditions);
        alertReceive.setCreatedAt(1568708777l);
        alertReceive.setDescription("Testing alert conversion");
        alertReceive.setId(654654l);
        alertReceive.setMd(false);
        alertReceive.setName("the_alert");
        alertReceive.setRearmPerSignal(true);
        alertReceive.setRearmSeconds(12);

        List<AlertReceiveService> services = new ArrayList<>();
        Map<String, String> settings = new HashMap<>();
        settings.put("setting1", "value_of_setting");
        services.add(new AlertReceiveService(858585l, "mail", settings, "the_service"));

        alertReceive.setServices(services);
        alertReceive.setUpdatedAt(1568708787l);
        alertReceive.setVersion(2);

        AlertSend result = instance.convertToSend(alertReceive);
        System.out.println("IN:  " + alertReceive);
        System.out.println("OUT: " + result);

        assertTrue(result.getActive());
        assertNotNull(result.getAttributes());
        assertEquals("run_book", result.getAttributes().getRunbookUrl());
        assertEquals("the_client", result.getClient());
        assertNotNull(result.getConditions());
        assertTrue(
                result.getConditions()
                        .stream()
                        .anyMatch(condition -> "testMetric".equals(condition.getMetricName())));
        assertTrue(
                result.getConditions()
                        .stream()
                        .anyMatch(condition -> condition.getTags()
                        .stream()
                        .anyMatch(tag -> CLIENT_TAG.equals(tag.getName())
                        && "the_client".equals(tag.getValues().get(0))
                        )));
        assertTrue(
                result.getConditions()
                        .stream()
                        .anyMatch(condition -> condition.getTags()
                        .stream()
                        .anyMatch(tag -> PROFILE_TAG.equals(tag.getName())
                        && "the_profile".equals(tag.getValues().get(0))
                        )));
        assertTrue(
                result.getConditions()
                        .stream()
                        .anyMatch(condition -> condition.getTags()
                        .stream()
                        .anyMatch(tag -> "the_tag".equals(tag.getName())
                        && "the_tag_value".equals(tag.getValues().get(0))
                        )));
        assertEquals("Testing alert conversion", result.getDescription());
        assertEquals(654654l, (long) result.getId());
        assertFalse(result.getMd());
        assertEquals("the_alert", result.getName());
        assertEquals("the_profile", result.getProfile());
        assertTrue(result.getRearmPerSignal());
        assertEquals(12, (int) result.getRearmSeconds());
        assertNotNull(result.getServices());
        assertTrue(result.getServices().stream().allMatch(serviceId -> serviceId == 858585l));
    }

}
